import random
import string
import cherrypy
from pymongo import MongoClient

class StringGenerator(object):
    @cherrypy.expose
    def index(self):
        return "Hello world!"

    @cherrypy.expose
    def generate(self, id1, task):
        print(id1)
        user = db.users.find_one({'id': int(id1)})
        task = db.task.find_one({"Task":task})
        if task != None:
            if not(str(id1) in task['Users_finished']):
                db.task.update_one({'_id':task['_id']},{'$push':{"Users_finished":id1}})
                coins = int(user['Coins']) + int(task['Price'])
                print(coins)
                db.users.update_one({'_id':user['_id']},{'$set':{"Coins": coins}})
                print(task['Link'])
                raise cherrypy.HTTPRedirect(task['Link'])
                return
        return 'Задания не существует или задание уже выполнено'

if __name__ == '__main__':
    client = MongoClient()
    db = client.botdb
    cherrypy.config.update({'server.socket_port': 8099})
    cherrypy.config.update({'server.socket_host': '31.148.99.122' } ) # Pub IP
    cherrypy.quickstart(StringGenerator())