#!/home/bot_reklama/bin
# -*- coding: utf-8 -*-

import config
import telebot
from telebot import types
import json
import cherrypy
from telebot import apihelper
from pymongo import MongoClient
import urllib3

http = urllib3.PoolManager()
admin=['271530596','410023817']
WEBHOOK_HOST='31.148.99.122'
WEBHOOK_PORT=8443
WEBHOOK_LISTEN='31.148.99.122'
WEBHOOK_SSL_CERT='/home/bot_reklama/webhook_cert.pem'
WEBHOOK_SSL_PRIV='/home/bot_reklama/webhook_pkey.pem'
WEBHOOK_URL_BASE="https://%s:%s" % (WEBHOOK_HOST, WEBHOOK_PORT)
WEBHOOK_URL_PATH='/%s/' % (config.token)

#apihelper.proxy = {'http':'http://67.205.149.230:3128'}

class WebhookServer(object):
    @cherrypy.expose
    def index(self):
        if 'content-length' in cherrypy.request.headers and \
           'content-type' in cherrypy.request.headers and \
            cherrypy.request.headers['content-type'] == 'application/json':
            length = int(cherrypy.request.headers['content-length'])
            json_string = cherrypy.request.body.read(length).decode("utf-8")
            update = telebot.types.Update.de_json(json_string)
            bot.process_new_updates([update])
            return ''
        else:
            raise cherrypy.HTTPError(403)

    @cherrypy.expose
    def generate(self, id1):
        print(id1)
        raise cherrypy.HTTPRedirect('http://google.ru')
        return

class Group_file:
    def __init__(self):
        self.data=''
        self.ind=None
        self.answer=None

    def open_file(self, fname):
        with open(fname, 'r', encoding='utf-8') as ids: #открываем файл на чтение
            self.data = json.load(ids) #загружаем из файла данные в словарь data	
        return(self.data)

class Auth:
    def __init__(self):
        self.flag = [False,False,False,False,False,False,False,False,False,\
        False,False,False,False,False,False,False, False, False, False, False,\
         False, False, False, False, False, False, False, False]
        self.group_dict = {}
        self.client_id = None
        self.group_step = None
        self.groupch_step = None
        self.task_step = None
        self.taskch_step = None
        self.task_dict = {}
        self.comm_step = None
        self.comm_dict = {}
        self.commch_step = None
    def set_flag(self, fl):
        self.flag[fl] = True
    def reset_flag(self, fl):
        self.flag[fl] = False
    def sost_flag(self):
        k=0
        for i in self.flag:
            #print('Состояние флага {} - {}'.format(k, self.flag[i]))
            k+=1
    def reset_dist(self):
        for i in range (0, 13):
            self.flag[i] = False
        self.group_dict.clear()
    
bot = telebot.TeleBot(config.token)
def keyb (args):
    keyboard = types.InlineKeyboardMarkup()
    keyboard.add(*[types.InlineKeyboardButton(text=name, callback_data=name) for name in args])
    return keyboard

def urlkeyb (tasks, uid):
    keyboard = types.InlineKeyboardMarkup()
    for task in tasks:
        if task == 'Назад':
            keyboard.add(types.InlineKeyboardButton(text=task, callback_data=task))
        else:
            url1 = 'http://31.148.99.122:8099/generate?id1={}&task={}'.format(uid, task) 
            keyboard.add(types.InlineKeyboardButton(text=task, url=url1))
    return keyboard

@bot.callback_query_handler(func=lambda a:True)
def check_answer(a):
    print('button')
    strin=''
    answer = a.data
    outstr=('Связаться','Назад')
    if answer=='Магазин':
        outstr=('Назад',)
        user = db.users.find_one({'id':a.message.chat.id})
        strin+='В магазине вы можете приобрести товары за накопленные монеты:'+'\n'
        strin+='Будущий товар'+'\n'+'\n'
        strin+='Ваш баланс: '+ str(user['Coins'])+'\n'
        bot.edit_message_text(
            chat_id=a.message.chat.id,
            message_id=a.message.message_id,
            text=strin,
            reply_markup=keyb(outstr),
            parse_mode='HTML'
            )

    if answer == 'Связаться':
        if not auten.flag[17]:
            bot.send_message(a.message.chat.id, "Начат чат с администратором")
            auten.set_flag(16)
            auten.set_flag(17)
            auten.client_id=a.message.chat.id
        else:
            bot.send_message(a.message.chat.id, "Администратор в данный момент занят. Попробуйте связаться с ним позже")
            
    if answer=='Отзывы':
        outstr=('Назад',)
        data=comen.open_file('/home/bot_reklama/comment_file.json')
        for i in data:
            strin+=i['comment']+'\n'
            strin+=i['name']+'\n'+'\n'
        bot.edit_message_text(
            chat_id=a.message.chat.id,
            message_id=a.message.message_id,
            text=strin,
            reply_markup=keyb(outstr),
            parse_mode='HTML'
            )

    if answer=='Задания':
        outstr=('Назад',)
        strin = ''
        for task in db.task.find():
            if str(a.message.chat.id) in admin:
                strin = ''
                strin+=task['Task'] + '\n'
                strin+='Описание:' + task['Description'] + '\n'
                strin+='Награда: ' + str(task['Price']) + ' монет' + '\n'
                strin+='Ссылка для выполнения задания:' + '\n'
                strin+='http://320745.msk-ovz.ru:8099/generate?id1={}&task={}'.format(a.message.chat.id, task['Task']) 
                strin+='\n'+ '\n'
                bot.send_message(a.message.chat.id, strin)
            else:
                if not (str(a.message.chat.id) in task['Users_finished']):
                    strin = ''
                    strin+=task['Task'] + '\n'
                    strin+='Описание:' + task['Description'] + '\n'
                    strin+='Награда: ' + str(task['Price']) + ' монет' + '\n'
                    strin+='Ссылка для выполнения задания:' + '\n'
                    strin+='http://320745.msk-ovz.ru:8099/generate?id1={}&task={}'.format(a.message.chat.id, task['Task']) 
                    strin+='\n'+ '\n'
                    bot.send_message(a.message.chat.id, strin)
        if strin == '':
            strin = 'Заданий больше нет' + '\n'
        else:
            strin = 'Для возврата нажмите назад' + '\n'
        bot.send_message(a.message.chat.id, strin, reply_markup=keyb(outstr))
    if answer=='Назад':
        outstr=('Рекламодателю','Заказчику','Правила','Отзывы', 'Задания', 'Магазин')
        strin="Выберите интересующую Вас категорию"
        bot.edit_message_text(
            chat_id=a.message.chat.id,
            message_id=a.message.message_id,
            text=strin,
            reply_markup=keyb(outstr),
            parse_mode='HTML'
            )
            
    if answer=='Рекламодателю':
        outstr=('Назад',)
        strin="По вопросам рекламы обращайтесь к администратору бота - @Zimaxik"
        bot.edit_message_text(
            chat_id=a.message.chat.id,
            message_id=a.message.message_id,
            text=strin,
            reply_markup=keyb(outstr),
            parse_mode='HTML'
            )
            
    if answer=='Правила':
        outstr=('Назад',)
        strin="Не материться"
        bot.edit_message_text(
            chat_id=a.message.chat.id,
            message_id=a.message.message_id,
            text=strin,
            reply_markup=keyb(outstr),
            parse_mode='HTML'
            )

    if answer=='Заказчику':
        data=daten.open_file('/home/bot_reklama/group_file.json')
        for i in data:
            strin+=i['Link']+' - '+i['Group_name']+'\n'
            strin+='<b>Тематика канала: </b>'+i['Description']+'\n'
            strin+='<b>Подписчиков: </b>'+i['Users']+'\n'
            strin+='<b>Охват поста: </b>'+i['Views']+'\n'
            strin+='<b>Цена: </b>'+i['Price']+'\n'
            strin+='<b>Условия размещения: </b>'+i['Conditions']+'\n'+'\n'
            #strin+='<b>Для связи: </b>'+'@Zimaxik'+'\n'+'\n'
        bot.edit_message_text(
            chat_id=a.message.chat.id,
            message_id=a.message.message_id,
            text=strin,
            reply_markup=keyb(outstr),
            parse_mode='HTML'
            )
            
    if answer=='Отменить':
        if auten.group_step!=None:
            auten.group_step-=1
        if auten._comm_step!=None:
            auten.comm_step-=1
        if auten.task_step!=None:
            auten.task_step-=1
        auten.reset_flag(24)
        bot.send_message(a.message.chat.id, "Действие отменено")
        
    if answer=="Подтвердить":
        if str(a.message.chat.id) in admin:
            if auten.taskch_step==1:
                outstr = []
                strin = 'Выберите задание для удаления' + '\n'
                for task in db.task.find():
                    strin+=task['Task'] + '\n'
                    strin+='Описание:' + task['Description'] + '\n'
                    strin+='Награда: ' + str(task['Price']) + ' монет' + '\n' + '\n'
                    outstr.append(task['Task'])
                outstr.append('Закончить')
                outstr = tuple(outstr)
                bot.edit_message_text(
                    chat_id=a.message.chat.id,
                    message_id=a.message.message_id,
                    text=strin,
                    reply_markup=keyb(outstr),
                    parse_mode='HTML'
                    )

            if auten.task_step==1:
                bot.send_message(a.message.chat.id, "Пароль веден, корректно. Введите название задания (слитно, без пробелов)")
                auten.task_step=2
            if auten.task_step==3:
                bot.send_message(a.message.chat.id, "Введите стоимость задания")
                auten.task_step=4
            if auten.task_step==5:
                bot.send_message(a.message.chat.id, "Введите ссылку на задание")
                auten.task_step=6
            if auten.task_step==7:
                bot.send_message(a.message.chat.id, "Введите описание задания")
                auten.task_step=8
            if auten.task_step==9:
                bot.send_message(a.message.chat.id, "Задание успешно сохранено")
                auten.task_dict["Users_finished"]=[]
                db.task.insert(auten.task_dict)
                auten.task_step=None

            if auten.group_step==1:
                bot.send_message(a.message.chat.id, "Пароль веден, корректно. Введите название группы:")
                auten.group_step=2
            if auten.group_step==3:
                bot.send_message(a.message.chat.id, "Введите количество подписчиков:")
                auten.group_step=4
            if auten.group_step==5:
                bot.send_message(a.message.chat.id, "Введите описание группы:")
                auten.group_step=6
            if auten.group_step==7:
                bot.send_message(a.message.chat.id, "Введите стоимость рекламы")
                auten.group_step=8
            if auten.group_step==9:
                bot.send_message(a.message.chat.id, "Введите охват поста")
                auten.group_step=10
            if auten.group_step==11:
                bot.send_message(a.message.chat.id, "Введите ссылку на группу")
                auten.group_step=12
            if auten.group_step==13:
                bot.send_message(a.message.chat.id, "Условия размещения")
                auten.group_step=14   
            if auten.group_step==15:
                outstr=('Рекламодателю','Заказчику','Правила','Отзывы', 'Задания', 'Магазин')
                with open('/home/bot_reklama/group_file.json', 'r', encoding='utf-8') as ids: #открываем файл на чтение
                    data = json.load(ids) #загружаем из файла данные в словарь data
                with open('/home/bot_reklama/group_file.json', 'w', encoding='utf8') as outfile:
                    data.append(auten.group_dict)
                    json.dump(data, outfile)
                bot.edit_message_text(
                    chat_id=a.message.chat.id,
                    message_id=a.message.message_id,
                    text='Данные успешно добавлены! Можете продолжать работу.',
                    reply_markup=keyb(outstr)
                    )
                auten.group_step=None
                
            if auten.comm_step==1:
                bot.send_message(a.message.chat.id, "Пароль веден, корректно. Введите никнейм человека:")
                auten.comm_step=2
            if auten.comm_step==3:
                bot.send_message(a.message.chat.id, "Введите комментарий человека:")
                auten.comm_step=4
            if auten.comm_step==5:
                outstr=('Рекламодателю','Заказчику','Правила','Отзывы', 'Задания', 'Магазин')
                with open('/home/bot_reklama/comment_file.json', 'r', encoding='utf-8') as ids: #открываем файл на чтение
                    data = json.load(ids) #загружаем из файла данные в словарь data
                with open('/home/bot_reklama/comment_file.json', 'w', encoding='utf8') as outfile:
                    data.append(auten.comm_dict)
                    json.dump(data, outfile)
                bot.edit_message_text(
                    chat_id=a.message.chat.id,
                    message_id=a.message.message_id,
                    text='Данные успешно добавлены! Можете продолжать работу.',
                    reply_markup=keyb(outstr)
                    )
                auten.comm_step=None
        
        if auten.flag[18]:
            if str(a.message.chat.id) in admin:
                auten.set_flag(19)
                auten.reset_flag(18)
                bot.send_message(a.message.chat.id, "Пароль введен верно. Введите сообщение. Оно будет отправлено всем пользователям.")
                
        if auten.groupch_step==1:
            if str(a.message.chat.id) in admin:
                auten.groupch_step=2

    if auten.taskch_step==1:
        task = db.task.find_one({"Task":answer})
        if task != None:
            outstr = []
            db.task.delete_one(task)
            strin = 'Выберите задание для удаления' + '\n'
        for task in db.task.find():
            strin+=task['Task'] + '\n'
            strin+='Описание:' + task['Description'] + '\n'
            strin+='Награда: ' + str(task['Price']) + ' монет' + '\n' + '\n'
            outstr.append(task['Task'])
        outstr.append('Закончить')
        outstr = tuple(outstr)
        bot.edit_message_text(
            chat_id=a.message.chat.id,
            message_id=a.message.message_id,
            text=strin,
            reply_markup=keyb(outstr),
            parse_mode='HTML'
            )

        if answer == 'Закончить':
            auten.taskch_step = None
            outstr=('Рекламодателю','Заказчику','Правила','Отзывы', 'Задания', 'Магазин')
            strin="Выберите интересующую Вас категорию"
            bot.edit_message_text(
                chat_id=a.message.chat.id,
                message_id=a.message.message_id,
                text=strin,
                reply_markup=keyb(outstr),
                parse_mode='HTML'
                )


    if auten.commch_step==1:
        if str(a.message.chat.id) in admin:
            data=aut2.open_file('/home/bot_reklama/comment_file.json')
            data_list=[]
            strin+=data[aut2.ind]['comment']+'\n'
            strin+=data[aut2.ind]['name']+'\n'+'\n'
            strin+='Выберите нужный пункт. Комментарий {} из {}'.format((aut2.ind+1), len(aut2.data))
            if aut2.ind == 0:
                data_list.append('Далее')
            else:
                data_list.insert(0, 'Ранее')
                data_list.append('Далее')
            data_list.append('Удалить')
            outstr=tuple(data_list)
            bot.edit_message_text(
                chat_id=a.message.chat.id,
                message_id=a.message.message_id,
                text=strin,
                reply_markup=keyb(outstr),
                parse_mode='HTML'
                )
            auten.commch_step=2
        
    if auten.commch_step==2:
        if str(a.message.chat.id) in admin:
            outstr=('Ок',)
            if answer=='Далее':
                if aut2.ind==(len(aut2.data)-1):
                    bot.edit_message_text(
                    chat_id=a.message.chat.id,
                    message_id=a.message.message_id,
                    text='Это последний комментарий',
                    reply_markup=keyb(outstr)
                    )
                    auten.commch_step=1
                else:
                    aut2.ind+=1
                    data_list=[]
                    strin+=aut2.data[aut2.ind]['comment']+'\n'
                    strin+=aut2.data[aut2.ind]['name']+'\n'+'\n'
                    strin+='Выберите нужный пункт. Комментарий {} из {}'.format((aut2.ind+1), len(aut2.data))
                    if aut2.ind == 0:
                        data_list.append('Далее')
                    else:
                        data_list.insert(0, 'Ранее')
                        data_list.append('Далее')
                    data_list.append('Удалить')
                    outstr=tuple(data_list)
                    bot.edit_message_text(
                        chat_id=a.message.chat.id,
                        message_id=a.message.message_id,
                        text=strin,
                        reply_markup=keyb(outstr),
                        parse_mode='HTML'
                        )
                        
            if answer=='Удалить':
                outstr=('Да','Нет')
                bot.edit_message_text(
                    chat_id=a.message.chat.id,
                    message_id=a.message.message_id,
                    text='Вы уверены, что хотите удалить комментарий ?',
                    reply_markup=keyb(outstr)
                    )
                    
            if answer=='Да':
                outstr=('Вернуться',)
                del aut2.data[aut2.ind]
                with open('/home/bot_reklama/comment_file.json', 'w', encoding='utf8') as outfile:
                    json.dump(aut2.data, outfile)
                bot.edit_message_text(
                    chat_id=a.message.chat.id,
                    message_id=a.message.message_id,
                    text='Комментарий успешно удален',
                    reply_markup=keyb(outstr)
                    )
                auten.commch_step=1
                aut2.ind=0
                
            if answer=='Нет':
                outstr=('Вернуться',)
                bot.edit_message_text(
                    chat_id=a.message.chat.id,
                    message_id=a.message.message_id,
                    text='Комментарий не будет удален',
                    reply_markup=keyb(outstr)
                    )
                auten.commch_step=1
                aut2.ind=0
                
            if answer=='Ранее':
                if aut2.ind==0:
                    bot.edit_message_text(
                        chat_id=a.message.chat.id,
                        message_id=a.message.message_id,
                        text='Это первый комментарий',
                        reply_markup=keyb(outstr)
                        )
                else:
                    aut2.ind-=1
                    data_list=[]
                    strin+=aut2.data[aut2.ind]['comment']+'\n'
                    strin+=aut2.data[aut2.ind]['name']+'\n'+'\n'
                    strin+='Выберите нужный пункт. Комментарий {} из {}'.format((aut2.ind+1), len(aut2.data))
                    if aut2.ind == 0:
                        data_list.append('Далее')
                    else:
                        data_list.insert(0, 'Ранее')
                        data_list.append('Далее')
                    data_list.append('Удалить')
                    outstr=tuple(data_list)
                    bot.edit_message_text(
                        chat_id=a.message.chat.id,
                        message_id=a.message.message_id,
                        text=strin,
                        reply_markup=keyb(outstr),
                        parse_mode='HTML'
                        )
                
    if auten.groupch_step==2:
        if str(a.message.chat.id) in admin:
            data=aut1.open_file('/home/bot_reklama/group_file.json')
            data_list=[]
            strin+=data[aut1.ind]['Link']+' - '+data[aut1.ind]['Group_name']+'\n'
            strin+='<b>Тематика канала: </b>'+data[aut1.ind]['Description']+'\n'
            strin+='<b>Подписчиков: </b>'+data[aut1.ind]['Users']+'\n'
            strin+='<b>Охват поста: </b>'+data[aut1.ind]['Views']+'\n'
            strin+='<b>Цена: </b>'+data[aut1.ind]['Price']+'\n'
            strin+='<b>Условия размещения: </b>'+data[aut1.ind]['Conditions']+'\n'+'\n'
            #strin+='<b>Для связи: </b>'+'@Zimaxik'+'\n'+'\n'
            strin+='Нажмите на пункт, который следует изменить. Группа {} из {}'.format((aut1.ind+1), len(data))
            for i in data[aut1.ind].keys():
                data_list.append(i)
            if aut1.ind == 0:
                data_list.append('Далее')
            else:
                data_list.insert(0, 'Ранее')
                data_list.append('Далее')
            data_list.append('Удалить')
            outstr=tuple(data_list)
            bot.edit_message_text(
                chat_id=a.message.chat.id,
                message_id=a.message.message_id,
                text=strin,
                reply_markup=keyb(outstr),
                parse_mode='HTML'
                )
            auten.groupch_step=3

    if auten.groupch_step==3:
        if str(a.message.chat.id) in admin:
            outstr=('Ок',)
            if answer in aut1.data[aut1.ind]:
                    auten.groupch_step=4
                    aut1.answer=answer
                    bot.send_message(a.message.chat.id, 'Отправьте новый текст для пункта {}'.format(answer))
           
            if answer=='Далее':
                if aut1.ind==(len(aut1.data)-1):
                    bot.edit_message_text(
                    chat_id=a.message.chat.id,
                    message_id=a.message.message_id,
                    text='Это последняя группа',
                    reply_markup=keyb(outstr)
                    )
                    auten.groupch_step=2
                else:
                    aut1.ind+=1
                    data_list=[]
                    strin+=aut1.data[aut1.ind]['Link']+' - '+aut1.data[aut1.ind]['Group_name']+'\n'
                    strin+='<b>Тематика канала: </b>'+aut1.data[aut1.ind]['Description']+'\n'
                    strin+='<b>Подписчиков: </b>'+aut1.data[aut1.ind]['Users']+'\n'
                    strin+='<b>Охват поста: </b>'+aut1.data[aut1.ind]['Views']+'\n'
                    strin+='<b>Цена: </b>'+aut1.data[aut1.ind]['Price']+'\n'
                    strin+='<b>Условия размещения: </b>'+aut1.data[aut1.ind]['Conditions']+'\n'+'\n'
                    #strin+='<b>Для связи: </b>'+'@Zimaxik'+'\n'+'\n'
                    strin+='Нажмите на пункт, который следует изменить. Группа {} из {}'.format((aut1.ind+1), len(aut1.data))
                    for i in aut1.data[aut1.ind].keys():
                        data_list.append(i)
                    if aut1.ind == 0:
                        data_list.append('Далее')
                    else:
                        data_list.insert(0, 'Ранее')
                        data_list.append('Далее')
                    data_list.append('Удалить')
                    outstr=tuple(data_list)
                    bot.edit_message_text(
                        chat_id=a.message.chat.id,
                        message_id=a.message.message_id,
                        text=strin,
                        reply_markup=keyb(outstr),
                        parse_mode='HTML'
                        )
                        
            if answer=='Удалить':
                outstr=('Да','Нет')
                bot.edit_message_text(
                    chat_id=a.message.chat.id,
                    message_id=a.message.message_id,
                    text='Вы уверены, что хотите удалить группу {} ?'.format(aut1.data[aut1.ind]['Group_name']),
                    reply_markup=keyb(outstr)
                    )
                    
            if answer=='Да':
                outstr=('Вернуться',)
                del aut1.data[aut1.ind]
                with open('/home/bot_reklama/group_file.json', 'w', encoding='utf8') as outfile:
                    json.dump(aut1.data, outfile)
                bot.edit_message_text(
                    chat_id=a.message.chat.id,
                    message_id=a.message.message_id,
                    text='Группа успешна удалена из списка',
                    reply_markup=keyb(outstr)
                    )
                auten.groupch_step=2
                aut1.ind=0
                
            if answer=='Нет':
                outstr=('Вернуться',)
                bot.edit_message_text(
                    chat_id=a.message.chat.id,
                    message_id=a.message.message_id,
                    text='Группа не будет удалена',
                    reply_markup=keyb(outstr)
                    )
                auten.groupch_step=2
                aut1.ind=0
                
            if answer=='Ранее':
                if aut1.ind==0:
                    bot.edit_message_text(
                        chat_id=a.message.chat.id,
                        message_id=a.message.message_id,
                        text='Это первая группа',
                        reply_markup=keyb(outstr)
                        )
                else:
                    aut1.ind-=1
                    data_list=[]
                    strin+=aut1.data[aut1.ind]['Link']+' - '+aut1.data[aut1.ind]['Group_name']+'\n'
                    strin+='<b>Тематика канала: </b>'+aut1.data[aut1.ind]['Description']+'\n'
                    strin+='<b>Подписчиков: </b>'+aut1.data[aut1.ind]['Users']+'\n'
                    strin+='<b>Охват поста: </b>'+aut1.data[aut1.ind]['Views']+'\n'
                    strin+='<b>Цена: </b>'+aut1.data[aut1.ind]['Price']+'\n'
                    strin+='<b>Условия размещения: </b>'+aut1.data[aut1.ind]['Conditions']+'\n'+'\n'
                    #strin+='<b>Для связи: </b>'+'@Zimaxik'+'\n'+'\n'
                    strin+='Нажмите на пункт, который следует изменить. Группа {} из {}'.format((aut1.ind+1), len(aut1.data))
                    for i in aut1.data[aut1.ind].keys():
                        data_list.append(i)
                    if aut1.ind == 0:
                        data_list.append('Далее')
                    else:
                        data_list.insert(0, 'Ранее')
                        data_list.append('Далее')
                    data_list.append('Удалить')
                    outstr=tuple(data_list)
                    bot.edit_message_text(
                        chat_id=a.message.chat.id,
                        message_id=a.message.message_id,
                        text=strin,
                        reply_markup=keyb(outstr),
                        parse_mode='HTML'
                        )




@bot.message_handler(commands=['start'])
def start_mess(message): #Приветствие
    if db.users.find_one({'id':message.chat.id}) == None:
        db.users.insert({'id':message.chat.id, 'Coins':10})
    wel_keyb=('Рекламодателю','Заказчику','Правила','Отзывы', 'Задания', 'Магазин')
    bot.send_message(message.chat.id, "Данный бот предназначен для покупки и продажи рекламы:", reply_markup=keyb(wel_keyb))

@bot.message_handler(commands=['task'])
def distr_message(message):
    if str(message.chat.id) in admin:
        auten.task_step=0
        auten.group_step = None
        auten.groupch_step = None
        auten.taskch_step = None
        auten.comm_step = None
        auten.commch_step = None
        bot.send_message(message.chat.id, "Добавить новое задание? Введите пароль")

@bot.message_handler(commands=['change_task'])
def distr_message(message):
    if str(message.chat.id) in admin:
        auten.taskch_step=0
        auten.group_step = None
        auten.groupch_step = None
        auten.task_step = None
        auten.comm_step = None
        auten.commch_step = None
        bot.send_message(message.chat.id, "Удалить задания? Введите пароль")

@bot.message_handler(commands=['distr'])
def distr_message(message):
    if str(message.chat.id) in admin:
        auten.set_flag(25)
        bot.send_message(message.chat.id, "Активировать массовую рассылку? Введите пароль")
        auten.comm_step= None
        auten.group_step = None
        auten.groupch_step = None
        auten.task_step = None
        auten.taskch_step = None
        auten.commch_step = None
        
@bot.message_handler(commands=['comment'])
def comment_add(message):
    if str(message.chat.id) in admin:
        auten.comm_step=0
        auten.group_step = None
        auten.groupch_step = None
        auten.task_step = None
        auten.taskch_step = None
        auten.commch_step = None
        bot.send_message(message.chat.id, "Добавить комментарий? Введите пароль")

@bot.message_handler(commands=['group'])
def group_message(message):
    if str(message.chat.id) in admin:
        auten.group_step=0
        auten.groupch_step = None
        auten.task_step = None
        auten.taskch_step = None
        auten.comm_step = None
        auten.commch_step = None
        bot.send_message(message.chat.id, "Введите пароль")

@bot.message_handler(commands=['stopchat'])
def stop_chat(message):
    auten.reset_flag(16)
    auten.reset_flag(17)
    bot.send_message(271530596, "Чат окончен")
    bot.send_message(auten.client_id, "Чат окончен")
    auten.client_id=None

@bot.message_handler(commands=['change_group'])
def change_group(message):
    if str(message.chat.id) in admin:
        outstr=('Подтвердить', 'Отменить')
        bot.send_message(message.chat.id, "Введите пароль")
        aut1.ind=0
        auten.groupch_step=0
        auten.group_step = None
        auten.task_step = None
        auten.taskch_step = None
        auten.comm_step = None
        auten.commch_step = None
@bot.message_handler(commands=['change_comment'])

def change_comment(message):
    if str(message.chat.id) in admin:
        outstr=('Подтвердить', 'Отменить')
        bot.send_message(message.chat.id, "Введите пароль")
        aut2.ind=0
        auten.commch_step=0
        auten.group_step = None
        auten.groupch_step = None
        auten.task_step = None
        auten.taskch_step = None
        auten.comm_step = None
        
        
@bot.message_handler(content_types=["text"])
def corr_pass(message):
    print('somethink')
    if auten.flag[16]:
            if message.chat.id==271530596:
                bot.send_message(auten.client_id, message.text)
            else:
                bot.send_message('271530596', message.text)

    if str(message.chat.id) in admin:
        if  auten.group_step==0:
            if message.text=='witcher3248':
                outstr=('Подтвердить', 'Отменить')
                auten.group_step=1
                bot.send_message(message.chat.id, "Введен пароль:" + message.text, reply_markup=keyb(outstr))
            else:
                auten.group_step=None
                bot.send_message(message.chat.id, "Пароль введен неверно!")
        if auten.group_step==2:
            auten.group_dict["Group_name"]=message.text
            auten.group_step=3
            outstr=('Подтвердить', 'Отменить')
            bot.send_message(message.chat.id, "Введен название группы:" + message.text, reply_markup=keyb(outstr))
        if auten.group_step==4:
            auten.group_dict["Users"]=message.text
            auten.group_step=5
            outstr=('Подтвердить', 'Отменить')
            bot.send_message(message.chat.id, "Введено количество подписчиков:" + message.text, reply_markup=keyb(outstr))
        if auten.group_step==6:
            auten.group_dict["Description"]=message.text
            auten.group_step=7
            outstr=('Подтвердить', 'Отменить')
            bot.send_message(message.chat.id, "Введено описание:" + message.text, reply_markup=keyb(outstr))
        if auten.group_step==8:
            auten.group_dict["Price"]=message.text
            auten.group_step=9
            outstr=('Подтвердить', 'Отменить')
            bot.send_message(message.chat.id, "Введена цена:" + message.text, reply_markup=keyb(outstr))
        if auten.group_step==10:
            auten.group_dict["Views"]=message.text
            auten.group_step=11
            outstr=('Подтвердить', 'Отменить')
            bot.send_message(message.chat.id, "Введен охват поста:" + message.text, reply_markup=keyb(outstr))
        if auten.group_step==12:
            auten.group_dict["Link"]=message.text
            auten.group_step=13
            outstr=('Подтвердить', 'Отменить')
            bot.send_message(message.chat.id, "Введена ссылка:" + message.text, reply_markup=keyb(outstr))
        if auten.group_step==14:
            auten.group_dict["Conditions"]=message.text
            auten.group_step=15
            outstr=('Подтвердить', 'Отменить')
            bot.send_message(message.chat.id, "Введены условия:" + message.text, reply_markup=keyb(outstr))
            
        if  auten.comm_step==0:
            if message.text=='witcher3248':
                outstr=('Подтвердить', 'Отменить')
                auten.comm_step=1
                bot.send_message(message.chat.id, "Введен пароль:" + message.text, reply_markup=keyb(outstr))
            else:
                auten.comm_step=None
                bot.send_message(message.chat.id, "Пароль введен неверно!")
        if auten.comm_step==2:
            auten.comm_dict["name"]=message.text
            auten.comm_step=3
            outstr=('Подтвердить', 'Отменить')
            bot.send_message(message.chat.id, "Введено имя человека:" + message.text, reply_markup=keyb(outstr))   
        if auten.comm_step==4:
            auten.comm_dict["comment"]=message.text
            auten.comm_step=5
            outstr=('Подтвердить', 'Отменить')
            bot.send_message(message.chat.id, "Введен комментарий человека:" + message.text, reply_markup=keyb(outstr))    
        
        if  auten.commch_step==0:
            if message.text=='witcher3248':
                outstr=('Подтвердить', 'Отменить')
                auten.commch_step=1
                bot.send_message(message.chat.id, "Введен пароль:" + message.text, reply_markup=keyb(outstr))
            else:
                auten.commch_step=None
                bot.send_message(message.chat.id, "Пароль введен неверно!")
        if auten.groupch_step==0:
            if message.text=='witcher3248':
                outstr=('Подтвердить',)
                auten.groupch_step=1
                bot.send_message(message.chat.id, "Введен пароль:" + message.text, reply_markup=keyb(outstr))
            else:
                auten.groupch_step==None
                bot.send_message(message.chat.id, "Пароль введен неверно!")
        if auten.flag[25]:
            if message.text=='witcher3248':
                outstr=('Подтвердить',)
                auten.set_flag(18)
                auten.reset_flag(25)
                bot.send_message(message.chat.id, "Введен пароль:" + message.text, reply_markup=keyb(outstr))
            else:
                auten.reset_flag(25)
                bot.send_message(message.chat.id, "Пароль введен неверно!")    
        if auten.flag[19]:
            data=db.users.distinct("id")
            for i in data:
                bot.send_message(i, message.text)
            auten.reset_flag(19)
        if auten.groupch_step==4:
            outstr=('Вернуться', 'Отменить')
            aut1.data[aut1.ind][aut1.answer]=message.text
            with open('/home/bot_reklama/group_file.json', 'w', encoding='utf8') as outfile:
                json.dump(aut1.data, outfile)
            auten.groupch_step=2
            bot.send_message(message.chat.id, "Данные успешно обновлены. Вернуться к редактированию?", reply_markup=keyb(outstr))
        
        if auten.task_step==0:
            if message.text=='witcher3248':
                outstr=('Подтвердить', 'Отменить')
                auten.task_step=1
                bot.send_message(message.chat.id, "Введен пароль: " + message.text, reply_markup=keyb(outstr))
            else:
                auten.task_step=None
                bot.send_message(message.chat.id, "Пароль введен неверно!")
        if auten.task_step==2:
            auten.task_dict["Task"]=message.text
            auten.task_step=3
            outstr=('Подтвердить', 'Отменить')
            bot.send_message(message.chat.id, "Введено название задания: " + message.text, reply_markup=keyb(outstr))
        if auten.task_step==4:
            auten.task_dict["Price"]=int(message.text)
            auten.task_step=5
            outstr=('Подтвердить', 'Отменить')
            bot.send_message(message.chat.id, "Введена стоимость задания: " + message.text, reply_markup=keyb(outstr))
        if auten.task_step==6:
            auten.task_dict["Link"]=message.text
            auten.task_step=7
            outstr=('Подтвердить', 'Отменить')
            bot.send_message(message.chat.id, "Введена ссылка на задание: " + message.text, reply_markup=keyb(outstr))
        if auten.task_step==8:
            auten.task_dict["Description"]=message.text
            auten.task_step=9
            outstr=('Подтвердить', 'Отменить')
            bot.send_message(message.chat.id, "Введено описание задания: " + message.text, reply_markup=keyb(outstr))

        if auten.taskch_step==0:
            if message.text=='witcher3248':
                outstr=('Подтвердить', 'Отменить')
                auten.taskch_step=1
                bot.send_message(message.chat.id, "Введен пароль: " + message.text, reply_markup=keyb(outstr))
            else:
                auten.taskch_step=None
                bot.send_message(message.chat.id, "Пароль введен неверно!")

if __name__ == '__main__':
    auten=Auth()
    aut1=Group_file()
    aut2=Group_file()
    daten=Group_file()
    comen=Group_file()
    filen=Group_file()
    client = MongoClient()
    db = client.botdb

    
    bot.remove_webhook()
    bot.set_webhook(url=WEBHOOK_URL_BASE + WEBHOOK_URL_PATH, certificate=open(WEBHOOK_SSL_CERT, 'r'))
    """access_log = cherrypy.log.access_log
    for handler in tuple(access_log.handlers):
        access_log.removeHandler(handler)"""
    cherrypy.config.update({
        'server.socket_host':WEBHOOK_LISTEN,
        'server.socket_port':WEBHOOK_PORT,
        'server.ssl_module':'builtin',
        'server.ssl_certificate':WEBHOOK_SSL_CERT,
        'server.ssl_private_key':WEBHOOK_SSL_PRIV
    })

   
    cherrypy.quickstart(WebhookServer(), WEBHOOK_URL_PATH, {'/': {}})  

